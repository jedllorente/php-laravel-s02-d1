<?php require_once "./code.php"?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <h1>While Loop</h1>
    <?php echo whileLoop();?>

    <h1>Do-While Loop</h1>
    <?php echo doWhileLoop();?>

    <h1>For Loop</h1>
    <?php echo forLoop();?>

    <h2>Continue and Break statement</h2>
    <?php modifiedForLoop();?>

    <h1>Array Manipulation</h1>

    <h2>Types of Arrays</h2>

    <h3>Simple Array</h3>

    <ul>
        <?php foreach($computerBrands as $brand) {?>
        <li><?php echo $brand; ?></li>
        <?php } ?>
    </ul>

    <!-- Associative Array -->
    <h2>Associative Array</h2>
    <ul>
        <?php foreach($gradePeriods as $period => $grade) {?>
        <!-- < ?= $period; ?> is < ?= $grade; ?> -->
        <!-- < ?= ?> can also be used for echoing only. -->
        <li>Grade in <?= $period;?> is <?= $grade;?></li>
        <?php } ?>
    </ul>

    <!-- Two dimensional array -->
    <h2>Two-dimensional array</h2>
    <ul>
        <?php foreach ($heroes as $team) {
            foreach($team as $member){
                echo "<li>".$member."</li>";
            }
        }?>
    </ul>

    <!-- Array Functions Sorting Array -->
    <h2>Array Functions</h2>

    <h3>Sorting Array Functions</h3>
    <pre><?php print_r($sortedBrands); ?></pre>

    <h3>Reverse Sorting Array Functions</h3>
    <pre><?php print_r($reverseSortedBrands); ?></pre>

    <?php array_push($computerBrands, 'Apple');?>
    <pre><?php print_r($computerBrands); ?></pre>

    <!-- Displaying a specific element in a 2-d array -->
    <p><?php echo $heroes[2][2];?></p>

</body>

</html>