<?php

// Repetition Control Structures

/*
While Loop
Do-while loop
For loop
*/

// While loop

function whileLoop(){
    $count = 5;

    while($count !== 0){
        echo $count.'<br/>';
        $count--;
    }
}

// Do-while loop

function doWhileLoop(){
    $count=10;
    do {
        echo $count.'<br/>';
        $count--;
    }while($count >0);
}

// For Loop

function forLoop(){
    for($i=0;$i<=20;$i++){
        echo $i.'<br/>';
    }
}

// Continue and Break Statements

/*
    Continues is a keyword that allows the code to go to the next loop withouth finishing the current code block.

    Break is a keyword that end the execution of the current loop.
 */

 function modifiedForLoop(){

    for($count=0; $count<=20; $count++){
        if($count % 2 === 0){
            continue;
        }
        echo $count.'<br/>';
        if($count > 10){
            break;
        }
    }
 }

 //Array Manipulation

 $studentNumbers = array('2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'); //before PHP 5.4

 $studentNumbers =['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']; // this format is introduced on PHP 5.4 and up.

 // Simple Arrays
 $grades = [98.5, 94.3, 89.2, 90.1];

 $computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'HP', 'Toshiba'];

 $tasks = [
     'drink html',
     'eat javascript',
     'inhale css',
     'bake sass'
 ];

//  Associative Array
$gradePeriods = ['firstGrading'=>'98.5', 'secondGrading'=>'94.3', 'thirdGrading'=>'89.2', 'fourthGrading'=>'90.1'];

// Two-dimensianal array
$heroes = [
    ['iron man', 'Hulk', 'thor', 'spiderman' ],
    ['wolverine', 'deadpool', 'phoenix', 'cyclops'],
    ['Batman', 'Night Wing', 'Green Lantern', 'Superman' ]
];

// 2-D associative array

$ironManPowers = [
    'regular'=>['repulsor blast','rocket punch'],
    'signature'=>['unibeam']
];


// Array Sorting
$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBrands);